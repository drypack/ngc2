# ngC2 #

## About ##

Lib with CodigoCriativo common components for AngularJS

## Requirements ##

- nodejs ^v6.*
- npm ^3.*
- gulp ^3.*

## Dependencies ##

- angular
- angular-material
- lodash
- moment
- mdDatepicker
- angular file upload
- alasql
- xlsx

## Installation ##

1- download via npm

```sh
cd {pasta-do-projeto}
npm install 'git@gitlab.com:drypack/ngc2.git' --save
```

2- Include the js and css files

```html
<link rel="stylesheet" href="node_modules/ng-c2/dist/ng-c2.min.css">
<script src="node_modules/ng-c2/dist/ng-c2.min.js"></script>
```

3- Include the ngC2 module as a dependence in your angular app

```javascript
var app = angular.module('app', ['ngC2']);
```

4- Add the javascripts and css dependencies in the gulpfile.js of your project

## Usage ##

See the examples in \example\index.html

## Directives ##

**Pagination**

```html
<c2-pagination paginator="{paginatorInstance}"></c2-pagination>
```

**Spinner**

```html
<c2-spinner
  bg-color="{colorName}"
  text-color="{colorName}">
</c2-spinner>
```

**Datapicker**

```html
<c2-date-time-picker
  ng-model="{model}"
  placeholderDate="{string}"
  placeholderTime="{string}"
  formatTime="{HH:mm A}"
  min-date="{date}" //ex: 2016-10-10 ou 11-02-2016
  max-date="{date}" //ex: 2016-10-10 ou 11-02-2016
  date-filter="{functionFilter}"
  disabled-date="{boolean}"
  disabled-time="{boolean}"
  open-on-click="{boolean}"
  with-time="{boolean}"
  auto-switch="{boolean}">
</c2-date-time-picker>
```

> For more information go to [mdPickers](https://github.com/alenaksu/mdPickers)

**Uploader Base64**

```html
<input type="file" c2-uploader-base64 ng-model="{{model}}" aria-label="{string}"/>
```

## Serviços ##

- C2Pagination
- C2Spinner
- C2Toast
- C2Dialog
- FileUploader
 
> Obs.: the C2Dialog uses the same options of $mdDialog do angular-material.
For more information go to: [Angular Material](https://material.angularjs.org/latest/api/service/$mdDialog)

**PrFile**

```javascript

var cars = [
 { name: 'Stilo',  brand: 'Fiat', date: moment().subtract(1, 'years') },
 { name: 'Punto',  brand: 'Fiat', date: moment().subtract(3, 'years') },
 { name: 'Fiesta', brand: 'Ford', date: '04-01-2017' }
]

PrFile.exportToExcel([
 { name: 'brand', label: 'Brand' },
 { name: 'name', label: 'Name' },
 { name: 'formatDate(date, \'DD-MM-YYYY\')', label: 'Bought in?' }
], cars, 'data-export', {
 orderBy: 'brand ASC, name ASC',
 where: 'brand like "%Fi%"'
});

```

## Aded icons ##

```html
  <md-icon md-svg-icon="c2-excel"></md-icon>
```

## Angular File Upload ##

### Mai directives ###

- nv-file-drop
- nv-file-over
- uploader
- over-class
- nv-file-select

> For more information go to [Angular File Upload](https://github.com/nervgh/angular-file-upload)

## How to contribute ##

- Install node and npm;
- Clone the project;
- run the following commands in the rot folder of the project:

```sh
cd {project-root-folder}
npm install -g gulp eslint eslint-plugin-angular
npm install
npm run build
```

- To run the examples, run the the following commands in another terminal:

```sh
cd {project-root-folder}
npm run server
```
- Browse to the link **http://localhost:5005/example**

