'use strict';

/*eslint angular/file-name: 0*/

(function () {
  'use strict';

  // Initialize the ngc2 module and define the dependencies

  angular.module('ngC2', ['ngMaterial', 'md.data.table', 'ngMaterialDatePicker', 'text-mask', 'angularFileUpload']);
})();
'use strict';

(function () {
  'use strict';

  config.$inject = ["$mdThemingProvider", "$mdIconProvider", "moment", "C2Icons", "$qProvider"];
  angular.module('ngC2').config(config);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function config($mdThemingProvider, $mdIconProvider, moment, C2Icons, $qProvider) {
    // Default settings for the components/libs used
    $mdThemingProvider.theme('default').primaryPalette('indigo').accentPalette('blue-grey');

    moment.locale('en-US');

    moment.createFromInputFallback = function (config) {
      // unreliable string magic, or
      config._d = new Date(config._i);
    };

    $qProvider.errorOnUnhandledRejections(false);

    angular.forEach(C2Icons, function (icon) {
      $mdIconProvider.icon(icon.id, icon.url);
    });
  }
})();
'use strict';

/*eslint angular/file-name: 0, no-undef: 0*/

(function () {
  'use strict';

  // Encapsulates the external libs to be loaded as angular dependencies

  angular.module('ngC2').constant('lodash', _).constant('_', _).constant('alasql', alasql).constant('moment', moment);
})();
'use strict';

(function () {
  'use strict';

  run.$inject = ["$templateCache", "C2Icons"];
  angular.module('ngC2').run(run);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function run($templateCache, C2Icons) {
    // Add the custom icon in the $template cache
    angular.forEach(C2Icons, function (icon) {
      $templateCache.put(icon.url, icon.svg);
    });
  }
})();
'use strict';

/*eslint-env es6*/

(function () {
  'use strict';

  /**
   * Directive that shows a custom date picker
   */
  /** @ngInject */

  angular.module('ngC2').component('c2DateTimePicker', {
    template: '\n          <md-input-container class="md-block" flex="$ctrl.flex">\n            <label for="$ctrl.id">{{$ctrl.label}}</label>\n            <md-icon md-font-set="material-icons" class="md-warn"\n              ng-style="$ctrl.style.label"\n              ng-click="$ctrl.show(true)"\n              md-colors="$ctrl.colors.icon">date_range</md-icon>\n            <input\n              id="$ctrl.id"\n              ng-model="$ctrl.ngModel"\n              ng-click="$ctrl.show(false)"\n              ng-readonly="$ctrl.clickToShowInField"\n              text-mask="$ctrl.maskConfig"\n              type="text">\n            </input>\n          </md-input-container>\n        ',
    controller: ['mdcDateTimeDialog', 'moment', 'DateFormat', 'Strings', function (mdcDateTimeDialog, moment, DateFormat, Strings) {
      var ctrl = this;

      ctrl.$onInit = function () {
        ctrl.style = {
          label: {
            cursor: 'pointer'
          }
        };

        ctrl.autoOk = setDefaultToTrue(ctrl.autoOk);
        ctrl.withMinutesPicker = setDefaultToTrue(ctrl.withMinutesPicker);
        ctrl.clickToShowInField = setDefaultToTrue(ctrl.clickToShowInField);
        ctrl.format = ctrl.format || ctrl.withTime ? DateFormat.ymdhm : DateFormat.ymd;
        ctrl.cancelText = angular.isDefined(ctrl.cancelText) ? ctrl.cancelText : Strings.cancelText;
        ctrl.todayText = angular.isDefined(ctrl.todayText) ? ctrl.todayText : Strings.todayText;

        ctrl.defineMask();
        ctrl.defineColors();
      };

      ctrl.defineColors = function () {
        ctrl.colors = {
          icon: {
            color: ctrl.iconColor ? ctrl.iconColor : 'default-grey-900'
          }
        };
      };

      ctrl.defineMask = function () {
        if (angular.isUndefined(ctrl.mask)) {
          ctrl.mask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];

          if (ctrl.withTime) {
            ctrl.mask = ctrl.mask.concat([' ', /\d/, /\d/, ':', /\d/, /\d/]);
          }
        }

        ctrl.maskConfig = {
          mask: ctrl.mask,
          guide: false
        };
      };

      function setDefaultToTrue(value) {
        return angular.isUndefined(value) || value;
      }

      ctrl.show = function (fromIcon) {
        if (ctrl.clickToShowInField || fromIcon) {
          mdcDateTimeDialog.show({
            maxDate: angular.isUndefined(ctrl.maxDate) ? null : ctrl.maxDate,
            format: ctrl.format,
            minutes: ctrl.withMinutesPicker,
            currentDate: ctrl.ngModel,
            autoOk: ctrl.autoOk,
            cancelText: ctrl.cancelText,
            todayText: ctrl.todayText,
            time: ctrl.withTime,
            date: true
          }).then(function (date) {
            ctrl.ngModel = moment(date).format(ctrl.format);
          });
        }
      };
    }],
    require: {
      ngModelCtrl: 'ngModel'
    },
    bindings: {
      label: '=',
      minDate: '=',
      maxDate: '=',
      ngModel: '<',
      id: '<',
      iconColor: '<',
      withTime: '<',
      withMask: '<',
      withMinutesPicker: '<',
      clickToShowInField: '<',
      autoOk: '<',
      format: '<',
      flex: '<',
      mask: '<',
      cancelText: '<',
      todayText: '<'
    }
  });
})();
'use strict';

/*eslint-env es6*/

(function () {
  'use strict';

  C2File.$inject = ["alasql", "lodash", "$filter"];
  angular.module('ngC2').factory('C2File', C2File);

  /**
   * Service que encapsula os serviços
   * de exportação e importação de arquivos
   */
  /** @ngInject */
  function C2File(alasql, lodash, $filter) {
    var obj = {
      exportToExcel: exportToExcel,
      exportToCSV: exportToCSV
    };

    //Defined filters

    alasql.fn.formatDate = function (date, format) {
      return $filter('c2Date')(date, format);
    };

    /**
     * Export the data to the selected format
     *
     * @param {string|array} fields
     *  String containing the fields separated by comma
     *    ex: name, description
     *    ex: name as Name, description as Description
     *    ex: *  (for all the fields)
     *  Array of objects with the fields
     *    ex: [
     *      { name: 'name', label: 'Name' },
     *      { name: 'description', label: 'Description' },
     *      { name: 'role.name', label: 'Role' },
     *      { name: 'formatDate(date)', label: 'Date' },*
     *    ]
     * @param {array} data - Array that work as data source
     * @param {string} fileName - File name without extension
     * @param {object} config
     * alasql configuration settings, like:
     *  - (boolean) headers
     * more the highlighted below
     *  - (string) where
     *  - (string) orderBy
     *
     * filters defined
     *
     * formatDate(campo, [format])
     *
     * Example:
     *
     * var cars = [
     *  { name: 'Stilo',  brand: 'Fiat', date: moment().subtract(1, 'years') },
     *  { name: 'Punto',  brand: 'Fiat', date: moment().subtract(3, 'years') },
     *  { name: 'Fiesta', brand: 'Ford', date: '2017-04-01'}
     * ]
     *
     * C2File.exportToExcel([
     *  { name: 'brand', label: 'Brand' },
     *  { name: 'name', label: 'Nome' },
     *  { name: 'formatDate(date, \'DD-MM-YYYY\')', label: 'Bought in?' }
     * ], cars, 'data-export', {
     *  orderBy: 'brand ASC, name ASC',
     *  where: 'brand like "%Fi%"'
     * });
     *
     * @returns {promise} - returns a promise that can be or not resolved
     */
    function exportTo(fields, data, fileName, config) {
      var defaultConfig = {
        headers: true
      };

      if (angular.isObject(config)) angular.merge(defaultConfig, config);
      if (angular.isUndefined(fileName)) fileName = 'export';

      defaultConfig.where = angular.isDefined(defaultConfig.where) ? ' WHERE ' + defaultConfig.where : ' ';
      defaultConfig.orderBy = angular.isDefined(defaultConfig.orderBy) ? ' ORDER BY ' + defaultConfig.orderBy : ' ';

      fileName = fileName + '.' + defaultConfig.formatTo;

      return alasql.promise('SELECT ' + buildFields(fields) + ' INTO ' + defaultConfig.formatTo.toUpperCase() + '(?, ?) FROM ? ' + defaultConfig.where + defaultConfig.orderBy, [fileName, defaultConfig, data]);
    }

    /**
     * Export the sent data as csv
     *
     * @see exportTo
     */
    function exportToCSV(fields, data, fileName, config) {
      if (angular.isUndefined(config)) config = {};

      config.formatTo = 'csv';

      return exportTo(fields, data, fileName, config);
    }

    /**
     * Export the sent data as excel
     *
     * @see exportTo
     */
    function exportToExcel(fields, data, fileName, config) {
      if (angular.isUndefined(config)) config = {};

      config.formatTo = 'xlsx';

      return exportTo(fields, data, fileName, config);
    }

    /**
     * Generate a string containing the fields to the SELECT da Query.
     */
    function buildFields(fields) {
      if (angular.isArray(fields)) {
        fields = lodash.map(fields, function (field) {
          return field.name + ' as [' + field.label + ']';
        }).join(', ');
      }

      return fields;
    }

    return obj;
  }
})();
'use strict';

/*eslint-env es6*/

(function () {
  'use strict';

  dialogService.$inject = ["$log", "$mdDialog", "$mdUtil", "$rootScope", "$animate", "$document", "Strings"];
  angular.module('ngC2').factory('C2Dialog', dialogService);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function dialogService($log, $mdDialog, $mdUtil, $rootScope, $animate, $document, Strings) {
    return {
      custom: custom,
      confirm: confirm,
      close: close,
      alert: alert
    };

    /**
     * Configures and do a merge between the passed object with the default settings
     * @param {object} config - Object containing the custom settings
     * @returns {dialogService} - Returns a service
     */
    function build(config) {

      if (!angular.isObject(config)) {
        $log.error('C2Dialog: invalid parameter. An object was expected.');
        return;
      }

      var defaultOptions = {
        hasBackdrop: false,
        escapeToClose: false,
        bindToController: true,
        clickOutsideToClose: true,
        autoWrap: true,
        skipHide: true,
        locals: {},
        zIndex: 75,
        fullscreen: false
      };

      // We don't do a merge with the local properties to avoid a the recursion limit
      if (angular.isDefined(config.locals)) {
        defaultOptions.locals = config.locals;
        delete config.locals;
      }

      return angular.merge(defaultOptions, config);
    }

    /**
     * Show an alert dialog and returns a promise that can be or not be resolved
     * @returns {promise}
     */
    function alert(config) {

      var options = build(config);

      options.locals = {
        title: angular.isDefined(options.title) ? options.title : 'Exception',
        description: angular.isDefined(options.description) ? options.description : '',
        okBgColor: angular.isDefined(options.okBgColor) ? options.okBgColor : 'red-A700',
        toolbarBgColor: angular.isDefined(options.toolbarBgColor) ? options.toolbarBgColor : 'red-A700',
        ok: angular.isDefined(options.ok) ? options.ok : Strings.ok
      };

      options.template = ' <md-dialog flex=50 aria-label="' + options.locals.title + '">\n              <md-toolbar md-scroll-shrink md-colors="::{background:\'default-{{ctrl.toolbarBgColor}}\'}">\n                <div class="md-toolbar-tools">\n                  <h3>\n                    <span>' + options.locals.title + '</span>\n                  </h3>\n                </div>\n              </md-toolbar>\n              <md-dialog-content layout-margin>\n                <p>' + options.locals.description + '</p>\n              </md-dialog-content>\n              <md-dialog-actions>\n                <md-button class="md-raised"\n                  md-colors="::{background:\'default-{{ctrl.okBgColor}}\'}"\n                  ng-click="ctrl.okAction()">' + options.locals.ok + '</md-button>\n              </md-dialog-actions>\n            </md-dialog>\n          ';

      options.controller = ['$mdDialog', function ($mdDialog) {
        var vm = this;

        vm.okAction = okAction;

        function okAction() {
          $mdDialog.hide();
        }
      }];

      options.controllerAs = 'ctrl';
      options.clickOutsideToClose = false;
      options.hasBackdrop = true;

      return $mdDialog.show(options);
    }

    /**
     * Show a confirmation dialog and return a promise that can be or not be resolved
     * @returns {promise}
     */
    function confirm(config) {

      var options = build(config);

      options.locals = {
        title: angular.isDefined(options.title) ? options.title : '',
        description: angular.isDefined(options.description) ? options.description : '',
        yesBgColor: angular.isDefined(options.yesBgColor) ? options.yesBgColor : 'primary',
        noBgColor: angular.isDefined(options.noBgColor) ? options.noBgColor : 'accent',
        yes: angular.isDefined(options.yes) ? options.yes : Strings.yes,
        no: angular.isDefined(options.no) ? options.no : Strings.no
      };

      options.template = ' <md-dialog flex=50 aria-label="{{::ctrl.title}}">\n              <md-toolbar md-scroll-shrink>\n                <div class="md-toolbar-tools">\n                  <h3>\n                    <span>{{::ctrl.title}}</span>\n                  </h3>\n                </div>\n              </md-toolbar>\n              <md-dialog-content layout-margin>\n                <p>{{::ctrl.description}}</p>\n              </md-dialog-content>\n              <md-dialog-actions>\n                <md-button class="md-raised"\n                  md-colors="::{background:\'default-{{ctrl.yesBgColor}}\'}"\n                  ng-click="ctrl.yesAction()">' + options.locals.yes + '</md-button>\n                <md-button class="md-raised"\n                  md-colors="::{background:\'default-{{ctrl.noBgColor}}\'}"\n                  ng-click="ctrl.noAction()">' + options.locals.no + '</md-button>\n              </md-dialog-actions>\n            </md-dialog>\n          ';

      options.controller = ['$mdDialog', function ($mdDialog) {
        var vm = this;

        vm.noAction = noAction;
        vm.yesAction = yesAction;

        function noAction() {
          $mdDialog.cancel();
        }
        function yesAction() {
          $mdDialog.hide();
        }
      }];

      options.controllerAs = 'ctrl';
      options.clickOutsideToClose = false;
      options.hasBackdrop = true;

      return $mdDialog.show(options);
    }

    /**
     * Show a custom dialog and return a promise that can be or not be resolved
     * @returns {promise}
     */
    function custom(config) {

      var options = build(config);

      if (angular.isUndefined(options.templateUrl) && angular.isUndefined(options.template)) {
        $log.error('C2Dialog: templateUrl ou template undefined. It is expected an templateUrl or a template.');
        return;
      }

      // Here the backdrop is created manually to  para decrease the z-index through a css class
      // The z-index has to be smaller because the dialog.confirm uses the original z-index of 80
      options = addBackdrop(options);

      options.hasBackdrop = false;

      return $mdDialog.show(options);
    }

    /**
     * Create the backdrop and show the content in a configurable z-index
     * to override the existing elements in the screen
     */
    function addBackdrop(options) {
      if (options.hasBackdrop) {
        var backdrop = $mdUtil.createBackdrop($rootScope, 'md-dialog-backdrop md-opaque md-backdrop-custom');

        $animate.enter(backdrop, angular.element($document.find('body')));

        var originalOnRemoving = options.onRemoving;

        // Executed  when the dialog closing animation ends
        options.onRemoving = function () {
          backdrop.remove();
          if (angular.isFunction(originalOnRemoving)) originalOnRemoving.call();
        };

        var originalOnComplete = options.onComplete;

        // Executed when the dialog opening animation ends
        options.onComplete = function (scope, element) {
          var zIndex = parseInt(options.zIndex, 10);

          angular.element($document[0].querySelector('.md-backdrop-custom')).css('z-index', zIndex);
          element.css('z-index', zIndex + 1);
          if (angular.isFunction(originalOnComplete)) originalOnComplete.call();
        };
      }

      return options;
    }

    /**
     * Close the dialog
     */
    function close() {
      $mdDialog.hide();
    }
  }
})();
'use strict';

(function () {

  'use strict';

  c2Date.$inject = ["moment", "DateFormat"];
  angular.module('ngC2').filter('c2Date', c2Date);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function c2Date(moment, DateFormat) {
    /**
     * Format a date in a specific format
     * @param value containing the date to be formated
     * @param inputFormat format of the input date
     * @param outputFormat format of the output date (can be null, the default will be used)
     */
    return function (value, inputFormat, outputFormat) {
      outputFormat = angular.isDefined(outputFormat) ? outputFormat : DateFormat.ymd;

      if (angular.isDefined(inputFormat)) {
        return moment(value, inputFormat).format(outputFormat);
      } else {
        return moment(value).format(outputFormat);
      }
    };
  }
})();
'use strict';

(function () {

  'use strict';

  c2Datetime.$inject = ["moment", "DateFormat"];
  angular.module('ngC2').filter('c2Datetime', c2Datetime);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function c2Datetime(moment, DateFormat) {
    /**
     * Format the date and time in a passed format
     * @param value - value to be formatted
     * @param format - output format (if not passed, will be used the default)
     */
    return function (value, format) {
      format = angular.isDefined(format) ? format : DateFormat.ymdhm;
      return moment(value).format(format);
    };
  }
})();
'use strict';

(function () {

  'use strict';

  real.$inject = ["$filter"];
  angular.module('ngC2').filter('real', real);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function real($filter) {
    /**
     * Format a currency value in the Brazilian pattern
     */
    return function (value) {
      return $filter('currency')(value, 'R$ ');
    };
  }
})();
'use strict';

/*eslint-env es6*/

(function () {
  'use strict';

  /* eslint-disable max-len */

  angular.module('ngC2').constant('C2Icons', [{
    id: 'c2-excel',
    url: 'c2-excel.svg',
    svg: '<svg height="24" width="24" style="fill:#2E7D32" viewBox="0 0 24 24"><path d="M6,2H14L20,8V20A2,2 0 0,1 18,22H6A2,2 0 0,1 4,20V4A2,2 0 0,1 6,2M13,3.5V9H18.5L13,3.5M17,11H13V13H14L12,14.67L10,13H11V11H7V13H8L11,15.5L8,18H7V20H11V18H10L12,16.33L14,18H13V20H17V18H16L13,15.5L16,13H17V11Z" /></svg>'
  }]);
})();
'use strict';

(function () {
  'use strict';

  angular.module('ngC2').factory('DateFormat', DateFormat);

  /**
   * Return the default date and date time formats
   */
  function DateFormat() {
    var service = {
      ymd: 'YYYY/MM/DD',
      ymdhm: 'YYYY/MM/DD HH:mm'
    };

    return service;
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('ngC2').factory('Strings', Strings);

  /**
   * Return the default strings for the components label
   */
  function Strings() {
    var service = {
      yes: 'Yes',
      no: 'No',
      ok: 'Ok',
      total: 'Total',
      items: 'Items',
      todayText: 'Today',
      cancelText: 'Cancel'
    };

    return service;
  }
})();
'use strict';

/*eslint-env es6*/

(function () {
  'use strict';

  angular.module('ngC2').directive('c2Pagination', paginationDirective);

  /**
   * Directive that shows a set of buttons and info of a pagination
   * Adopts the an style similar to the one that Google uses
   * Uses material designer elements
   */
  /** @ngInject */
  function paginationDirective() {
    return {
      restrict: 'AE',
      scope: {
        paginator: '='
      },
      template: '\n        <section class="c2-pagination" layout="row">\n          <section layout="row" layout-align="center center" layout-wrap\n            style="margin-right: 10px"\n            ng-show="paginator.numberOfPages > 1">\n              <md-button class="md-raised"\n                ng-disabled="paginator.currentPage === 1"\n                ng-click="paginator.goToPage(1)">{{paginator.options.labels.first}}</md-button>\n              <md-button class="md-raised"\n                ng-disabled="paginator.currentPage === 1"\n                ng-click="paginator.previousPage()">{{paginator.options.labels.previous}}</md-button>\n              <md-button class="md-raised"\n                ng-repeat="n in paginator.pages(s)"\n                ng-class="{\'md-primary\': n == paginator.currentPage}"\n                ng-click="paginator.goToPage(n)"\n                ng-bind="n">1</md-button>\n            <md-button class="md-raised"\n              ng-disabled="paginator.currentPage == paginator.numberOfPages"\n              ng-click="paginator.nextPage()">{{paginator.options.labels.next}}</md-button>\n            <md-button class="md-raised"\n              ng-disabled="paginator.currentPage == paginator.numberOfPages"\n              ng-click="paginator.goToPage(paginator.numberOfPages)">{{paginator.options.labels.last}}</md-button>\n          </section>\n          <section layout="row" layout-align="center center"\n            ng-show="paginator.total > 0">\n            <md-button class="md-raised" style="cursor: default;"\n              ng-disabled="true" md-colors="::{background:\'default-accent\'}">{{paginator.options.labels.total}}: {{paginator.total}} {{paginator.options.labels.items}}</md-button>\n          </section>\n        </section>'
    };
  }
})();
'use strict';

/*eslint-env es6*/

(function () {
  'use strict';

  paginationService.$inject = ["Strings"];
  angular.module('ngC2').factory('C2Pagination', paginationService);

  /** @ngInject */
  function paginationService(Strings) {

    /**
     * Create and return a paginator object
     *
     * @constructor
     * @param {function} searchMethod - responsible for loading the data and that will be called when a given page button is clicked
     * @param {int} perPage - Number os pages. The default is 10.
     * @param {object} _options - Object containing the additional options/settings
     */
    var C2Paging = function C2Paging(searchMethod, perPage, _options) {

      var options = {
        maxPages: 10,
        labels: {
          first: '««',
          previous: '«',
          next: '»',
          last: '»»',
          total: Strings.total, // default, can be overwritten
          items: Strings.items // default, can be overwritten
        }

        // overrides the default settings
      };if (angular.isUndefined(perPage)) {
        perPage = 10;
      }

      if (angular.isDefined(_options)) {
        if (angular.isDefined(_options.maxPages)) {
          options.maxPages = _options.maxPages;
        }
        if (angular.isDefined(_options.labels)) {
          angular.merge(options.labels, _options.labels);
        }
      }

      // Calc how many pages are gonna be shown in the intermediate section of the pagination (the pages with numbers)
      options.maxPagesInner = Math.floor(options.maxPages / 2);

      // Create the paginator object with the initial parameters
      this.searchMethod = searchMethod;
      this.numberOfPages = 1;
      this.total = 0;
      this.perPage = perPage;
      this.currentPage = 0;
      this.options = options;
    };

    /**
     * Calc the number os pages to be shown based in the total of items and the perPage value
     *
     * @param {int} total - total of items
     */
    C2Paging.prototype.calcNumberOfPages = function (total) {
      this.total = total;

      if (total <= 0) {
        this.numberOfPages = 1;
      } else {
        this.numberOfPages = Math.floor(total / this.perPage) + (total % this.perPage > 0 ? 1 : 0);
      }
    };

    /**
     * Determine the pages that must be shown
     */
    C2Paging.prototype.pages = function () {
      var ret = [];

      for (var i = 1; i <= this.numberOfPages; i++) {
        if (this.currentPage === i) {
          ret.push(i);
        } else {
          if (this.currentPage <= this.options.maxPagesInner + 1) {
            if (i <= this.options.maxPages) {
              ret.push(i);
            }
          } else {
            if (i >= this.currentPage - this.options.maxPagesInner && i <= this.currentPage + this.options.maxPagesInner) {
              ret.push(i);
            }
          }
        }
      }
      return ret;
    };

    /**
     * Load the data of the previous page
     */
    C2Paging.prototype.previousPage = function () {
      if (this.currentPage > 1) {
        this.searchMethod(this.currentPage - 1);
      }
    };

    /**
     * Load the data of the next page
     */
    C2Paging.prototype.nextPage = function () {
      if (this.currentPage < this.numberOfPages) {
        this.searchMethod(this.currentPage + 1);
      }
    };

    /**
     * Load the data of the selected page
     *
     * @param {int} page - page to be loaded
     */
    C2Paging.prototype.goToPage = function (page) {
      if (page >= 1 && page <= this.numberOfPages) {
        this.searchMethod(page);
      }
    };

    return {
      getInstance: function getInstance(searchMethod, perPage, _options) {
        return new C2Paging(searchMethod, perPage, _options);
      }
    };
  }
})();
'use strict';

/*eslint-env es6*/

(function () {
  'use strict';

  /**
   * Directive that shows a spinner every time that the broadcast is fired
   */
  /** @ngInject */

  angular.module('ngC2').component('c2Spinner', {
    template: '\n        <md-progress-linear class="spin-label-component {{::$ctrl.color}}"\n          ng-style="$ctrl.style"\n          md-mode="indeterminate"\n          ng-show="$ctrl.spinner && $ctrl.spinner.show"></md-progress-linear>\n        ',
    bindings: {
      position: '@',
      color: '@'
    },
    controller: ['$scope', function ($scope) {
      var ctrl = this;

      ctrl.$onInit = function () {
        //Defines the position
        ctrl.style = { position: angular.isDefined(ctrl.position) ? ctrl.position : 'fixed' };
        if (angular.isUndefined(ctrl.color)) ctrl.color = 'md-primary';
      };
      // Default behavior
      ctrl.spinner = {
        show: false
      };

      // Listen to the show broadcast event
      $scope.$on('show-spinner', function () {
        ctrl.spinner = {
          show: true
        };
      });

      // Listen to the hide broadcast event
      $scope.$on('hide-spinner', function () {
        ctrl.spinner = {
          show: false
        };
      });
    }]
  });
})();
'use strict';

(function () {
  'use strict';

  spinnerService.$inject = ["$rootScope"];
  angular.module('ngC2').factory('C2Spinner', spinnerService);

  /** @ngInject */
  function spinnerService($rootScope) {
    return {
      show: show,
      hide: hide
    };

    /**
     * Show the spinner
     */
    function show() {
      // Send the o signal to show the spinner
      $rootScope.$broadcast('show-spinner');
    }

    /**
     * Hide the spinner
     */
    function hide() {
      // Send the o signal to show the spinner
      $rootScope.$broadcast('hide-spinner');
    }
  }
})();
'use strict';

/*eslint-env es6*/

(function () {
  'use strict';

  Toast.$inject = ["$mdToast", "lodash", "$log", "C2Dialog"];
  angular.module('ngC2').factory('C2Toast', Toast);

  /**
   * Encapsulate the angular toast component adding custom options
   */
  /** @ngInject */
  function Toast($mdToast, lodash, $log, C2Dialog) {
    var obj = {
      success: success,
      error: error,
      errorValidation: errorValidation,
      info: info,
      warn: warn,
      hide: hide
    };

    /**
     * Show a notification
     *
     * @param {string} msg - Message to be shown
     * @param {string} color - Message background color
     * @returns {promisse} - return promise that can be or not resolved
     */
    function toast(msg, color, options) {
      if (msg) {
        var defaultOptions = {
          template: '\n            <md-toast>\n              <div class="md-toast-content" md-colors="::{background:\'' + color + '\'}">\n                <span class="md-toast-text" flex>' + msg + '</span>\n              </div>\n            </md-toast>\n          ',
          position: 'top right'
        };

        if (angular.isObject(options)) defaultOptions = angular.merge(defaultOptions, options);

        return $mdToast.show(defaultOptions);
      } else {
        $log.debug('Message to be shown not passed');
      }
    }

    /**
     * Hide a notification
     *
     * @param {object} object - An additional argument to resolve the promise
     *
     * @returns {promise} - Returns a promise that is executed when the notification is removed from the DOM
     */
    function hide(object) {
      return $mdToast.hide(object);
    }

    function success(msg, options) {
      return toast(msg, 'green', options);
    }

    /**
     * Show a toast with error messages related to validation
     *
     * @param {string | object | array} errors - String, Object or Array containing the error messages
     * Supports two different formats
     *
     * To validate attributes
     *   {
     *    "attribute-name1": ["error message 1", "error message 2"],
     *    "attribute-name2": ["error message 1"]
     *   }
     *
     * or - To multiple simple messages
     *   [
     *    'Message 1',
     *    'Message 2'
     *   ]
     *
     *  or - for a single Message
     *   "Message 1"
     *
     *  or - for exceptions - in this case a C2Dialog
     *   {
     *     message: 'Unknown Exception',
     *     source: 'DryPack.Controllers.SupportController.langs()',
     *     line: 21,
     *     stacktrace: 'at DryPack.Controllers.SupportController.langs() in /home/workspa...'
     *   }
     * @param {string} msg - Optional. error Message
     */
    function error(errors) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      // if it is an object containing error, iterate over its attributes
      // to show the error messages assigned
      if (angular.isObject(errors)) {
        var errorStr = '';

        if (checkErrorIsUnknownException(errors)) {
          errorStr += 'Message: ' + errors.message + '<br/>Source: ' + errors.source + '<br/>Line: ' + errors.line + '<br/><br/>StackTrace: ';

          var steps = errors.stacktrace.split(' at ');

          if (steps.length > 0) {
            steps.forEach(function (step, index) {
              errorStr += '<br/>Step ' + (index + 1) + ': ' + step;
            });
          } else {
            errorStr += errors.stacktrace;
          }

          return C2Dialog.alert({
            title: 'Exception',
            description: errorStr
          });
        } else {
          // Show the error messages contained in the object
          //  {
          //    "name": ["the field is mandatory"],
          //    "password": [
          //      " The password confirmation doe snot match.",
          //      " The password must have at least 6 characters."
          //    ]
          //  }
          lodash.forIn(errors, function (keyErrors) {
            errorStr += buildArrayMessage(keyErrors);
          });
        }

        errors = errorStr;
      } else {
        if (angular.isArray(errors)) {
          errors = buildArrayMessage(errors);
        }
      }

      return toast(errors, 'red-A700', options);
    }

    function buildArrayMessage(arrMsg) {
      var msg = '';

      if (angular.isArray(arrMsg)) {
        // Iterate over the attribute errors
        arrMsg.forEach(function (error) {
          msg += error + '<br/>';
        });
      } else {
        msg += arrMsg + '<br/>';
      }

      return msg;
    }

    /**
     * show a toast with the validation error messages related to it
     *
     * @param {object | array} errors - Object or Array containing the error messages. Must be in the following format:
     *   {
     *    "attribute-name1": ["error message 1", "error message 2"],
     *    "attribute-name2": ["error message 1"]
     *   }
     * or
     *   [
     *    'Message 1',
     *    'Message 2'
     *   ]
     * @param {string} msg - Optional. Error message.
     * @param {object} options - default $mdToast options
     */
    function errorValidation(errors, msg, options) {
      obj.error(angular.isArray(errors) || angular.isObject(errors) ? errors : msg, options);
    }

    function info(msg, options) {
      return toast(msg, 'teal', options);
    }

    function warn(msg, options) {
      return toast(msg, 'warn', options);
    }

    function checkErrorIsUnknownException(errors) {
      return angular.isObject(errors) && angular.isDefined(errors.line) && angular.isDefined(errors.source) && angular.isDefined(errors.stacktrace);
    }

    return obj;
  }
})();
'use strict';

/*eslint-env es6*/

(function () {
  'use strict';

  angular.module('ngC2').directive('c2UploaderBase64', ["$q", function ($q) {
    var slice = Array.prototype.slice;

    return {
      restrict: 'A',
      require: '?ngModel',
      link: function link(scope, element, attrs, ngModel) {
        if (!ngModel) return;

        ngModel.$render = function () {};

        element.bind('change', function (e) {
          var element = e.target;

          $q.all(slice.call(element.files, 0).map(readFile)).then(function (values) {
            if (element.multiple) ngModel.$setViewValue(values);else ngModel.$setViewValue(values.length ? values[0] : null);
          });

          function readFile(file) {
            var deferred = $q.defer();

            var reader = new FileReader();

            reader.onload = function (e) {
              deferred.resolve(e.target.result);
            };
            reader.onerror = function (e) {
              deferred.reject(e);
            };
            reader.readAsDataURL(file);

            return deferred.promise;
          }
        });
      }
    };
  }]);
})();