/*eslint-env es6*/

(function() {
  'use strict';

  angular.module('ngC2')
    .factory('C2Pagination', paginationService);

  /** @ngInject */
  function paginationService(Strings) {

    /**
     * Create and return a paginator object
     *
     * @constructor
     * @param {function} searchMethod - responsible for loading the data and that will be called when a given page button is clicked
     * @param {int} perPage - Number os pages. The default is 10.
     * @param {object} _options - Object containing the additional options/settings
     */
    var C2Paging = function(searchMethod, perPage, _options) {

      var options = {
        maxPages: 10,
        labels: {
          first: '««',
          previous: '«',
          next: '»',
          last: '»»',
          total: Strings.total, // default, can be overwritten
          items: Strings.items // default, can be overwritten
        }
      }

      // overrides the default settings
      if (angular.isUndefined(perPage)) {
        perPage = 10;
      }

      if (angular.isDefined(_options)) {
        if (angular.isDefined(_options.maxPages)) {
          options.maxPages = _options.maxPages;
        }
        if (angular.isDefined(_options.labels)) {
          angular.merge(options.labels, _options.labels);
        }
      }

      // Calc how many pages are gonna be shown in the intermediate section of the pagination (the pages with numbers)
      options.maxPagesInner = Math.floor(options.maxPages / 2);

      // Create the paginator object with the initial parameters
      this.searchMethod = searchMethod;
      this.numberOfPages = 1;
      this.total = 0;
      this.perPage = perPage;
      this.currentPage = 0;
      this.options = options;
    }

    /**
     * Calc the number os pages to be shown based in the total of items and the perPage value
     *
     * @param {int} total - total of items
     */
    C2Paging.prototype.calcNumberOfPages = function(total) {
      this.total = total;

      if (total <= 0) {
        this.numberOfPages = 1
      } else {
        this.numberOfPages = Math.floor(total / this.perPage) + (total % this.perPage > 0 ? 1 : 0);
      }
    };

    /**
     * Determine the pages that must be shown
     */
    C2Paging.prototype.pages = function() {
      var ret = [];

      for (var i = 1; i <= this.numberOfPages; i++) {
        if (this.currentPage === i) {
          ret.push(i);
        } else {
          if (this.currentPage <= (this.options.maxPagesInner + 1)) {
            if (i <= this.options.maxPages) {
              ret.push(i);
            }
          } else {
            if ((i >= this.currentPage - this.options.maxPagesInner) &&
              (i <= this.currentPage + this.options.maxPagesInner)) {
              ret.push(i);
            }
          }
        }
      }
      return ret;
    };

    /**
     * Load the data of the previous page
     */
    C2Paging.prototype.previousPage = function() {
      if (this.currentPage > 1) {
        this.searchMethod(this.currentPage - 1);
      }
    };

    /**
     * Load the data of the next page
     */
    C2Paging.prototype.nextPage = function() {
      if (this.currentPage < this.numberOfPages) {
        this.searchMethod(this.currentPage + 1);
      }
    };

    /**
     * Load the data of the selected page
     *
     * @param {int} page - page to be loaded
     */
    C2Paging.prototype.goToPage = function(page) {
      if (page >= 1 && page <= this.numberOfPages) {
        this.searchMethod(page);
      }
    };

    return {
      getInstance: function(searchMethod, perPage, _options) {
        return new C2Paging(searchMethod, perPage, _options);
      }
    };
  }

})();
