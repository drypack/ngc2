/*eslint-env es6*/

(function() {
  'use strict';

  angular.module('ngC2')
    .directive('c2Pagination', paginationDirective);

  /**
   * Directive that shows a set of buttons and info of a pagination
   * Adopts the an style similar to the one that Google uses
   * Uses material designer elements
   */
  /** @ngInject */
  function paginationDirective() {
    return {
      restrict: 'AE',
      scope: {
        paginator: '='
      },
      template: `
        <section class="c2-pagination" layout="row">
          <section layout="row" layout-align="center center" layout-wrap
            style="margin-right: 10px"
            ng-show="paginator.numberOfPages > 1">
              <md-button class="md-raised"
                ng-disabled="paginator.currentPage === 1"
                ng-click="paginator.goToPage(1)">{{paginator.options.labels.first}}</md-button>
              <md-button class="md-raised"
                ng-disabled="paginator.currentPage === 1"
                ng-click="paginator.previousPage()">{{paginator.options.labels.previous}}</md-button>
              <md-button class="md-raised"
                ng-repeat="n in paginator.pages(s)"
                ng-class="{'md-primary': n == paginator.currentPage}"
                ng-click="paginator.goToPage(n)"
                ng-bind="n">1</md-button>
            <md-button class="md-raised"
              ng-disabled="paginator.currentPage == paginator.numberOfPages"
              ng-click="paginator.nextPage()">{{paginator.options.labels.next}}</md-button>
            <md-button class="md-raised"
              ng-disabled="paginator.currentPage == paginator.numberOfPages"
              ng-click="paginator.goToPage(paginator.numberOfPages)">{{paginator.options.labels.last}}</md-button>
          </section>
          <section layout="row" layout-align="center center"
            ng-show="paginator.total > 0">
            <md-button class="md-raised" style="cursor: default;"
              ng-disabled="true" md-colors="::{background:'default-accent'}">{{paginator.options.labels.total}}: {{paginator.total}} {{paginator.options.labels.items}}</md-button>
          </section>
        </section>`
    };
  }

})();
