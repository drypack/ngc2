(function() {

  'use strict';

  angular
    .module('ngC2')
    .filter('real', real);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function real($filter) {
    /**
     * Format a currency value in the Brazilian pattern
     */
    return function(value) {
      return $filter('currency')(value, 'R$ ');
    }
  }

})();
