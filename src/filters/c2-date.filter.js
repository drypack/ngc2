(function() {

  'use strict';

  angular
    .module('ngC2')
    .filter('c2Date', c2Date);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function c2Date(moment, DateFormat) {
    /**
     * Format a date in a specific format
     * @param value containing the date to be formated
     * @param inputFormat format of the input date
     * @param outputFormat format of the output date (can be null, the default will be used)
     */
    return function(value, inputFormat, outputFormat) {
      outputFormat = angular.isDefined(outputFormat) ? outputFormat : DateFormat.ymd;

      if (angular.isDefined(inputFormat)) {
        return moment(value, inputFormat).format(outputFormat);
      } else {
        return moment(value).format(outputFormat);
      }
    }
  }

})();
