(function() {

  'use strict';

  angular
    .module('ngC2')
    .filter('c2Datetime', c2Datetime);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function c2Datetime(moment, DateFormat) {
    /**
     * Format the date and time in a passed format
     * @param value - value to be formatted
     * @param format - output format (if not passed, will be used the default)
     */
    return function(value, format) {
      format = angular.isDefined(format) ? format : DateFormat.ymdhm;
      return moment(value).format(format);
    }
  }

})();
