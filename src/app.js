/*eslint angular/file-name: 0*/

(function() {
  'use strict';

  // Initialize the ngc2 module and define the dependencies
  angular.module('ngC2', ['ngMaterial', 'md.data.table', 'ngMaterialDatePicker', 'text-mask', 'angularFileUpload']);

})();
