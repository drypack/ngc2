/*eslint-env es6*/

(function() {
  'use strict';

  /**
   * Directive that shows a spinner every time that the broadcast is fired
   */
  /** @ngInject */
  angular
    .module('ngC2')
    .component('c2Spinner', {
      template: `
        <md-progress-linear class="spin-label-component {{::$ctrl.color}}"
          ng-style="$ctrl.style"
          md-mode="indeterminate"
          ng-show="$ctrl.spinner && $ctrl.spinner.show"></md-progress-linear>
        `,
      bindings: {
        position: '@',
        color: '@'
      },
      controller: ['$scope', function($scope) {
        var ctrl = this;

        ctrl.$onInit = function() {
          //Defines the position
          ctrl.style = { position: angular.isDefined(ctrl.position) ? ctrl.position : 'fixed' };
          if (angular.isUndefined(ctrl.color)) ctrl.color = 'md-primary';
        };
        // Default behavior
        ctrl.spinner = {
          show: false
        };

        // Listen to the show broadcast event
        $scope.$on('show-spinner', function() {
          ctrl.spinner = {
            show: true
          };
        });

        // Listen to the hide broadcast event
        $scope.$on('hide-spinner', function() {
          ctrl.spinner = {
            show: false
          };
        });
      }]
    });

})();
