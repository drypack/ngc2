(function() {
  'use strict';

  angular.module('ngC2')
    .factory('C2Spinner', spinnerService);

  /** @ngInject */
  function spinnerService($rootScope) {
    return {
      show: show,
      hide: hide
    };

    /**
     * Show the spinner
     */
    function show() {
      // Send the o signal to show the spinner
      $rootScope.$broadcast('show-spinner');
    }


    /**
     * Hide the spinner
     */
    function hide() {
      // Send the o signal to show the spinner
      $rootScope.$broadcast('hide-spinner');
    }
  }

})();
