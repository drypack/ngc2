/*eslint angular/file-name: 0, no-undef: 0*/

(function() {
  'use strict';

  // Encapsulates the external libs to be loaded as angular dependencies
  angular
    .module('ngC2')
    .constant('lodash', _)
    .constant('_', _)
    .constant('alasql', alasql)
    .constant('moment', moment);
})();
