(function () {
  'use strict';

  angular
    .module('ngC2')
    .run(run);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function run($templateCache, C2Icons) {
    // Add the custom icon in the $template cache
    angular.forEach(C2Icons, function(icon) {
      $templateCache.put(icon.url, icon.svg);
    });
  }
}());


