/*eslint-env es6*/

(function() {
  'use strict';

  angular
    .module('ngC2')
    .factory('C2File', C2File);

  /**
   * Service que encapsula os serviços
   * de exportação e importação de arquivos
   */
  /** @ngInject */
  function C2File(alasql, lodash, $filter) {
    var obj = {
      exportToExcel: exportToExcel,
      exportToCSV: exportToCSV
    };

    //Defined filters

    alasql.fn.formatDate = function(date, format) {
      return $filter('c2Date')(date, format);
    }

    /**
     * Export the data to the selected format
     *
     * @param {string|array} fields
     *  String containing the fields separated by comma
     *    ex: name, description
     *    ex: name as Name, description as Description
     *    ex: *  (for all the fields)
     *  Array of objects with the fields
     *    ex: [
     *      { name: 'name', label: 'Name' },
     *      { name: 'description', label: 'Description' },
     *      { name: 'role.name', label: 'Role' },
     *      { name: 'formatDate(date)', label: 'Date' },*
     *    ]
     * @param {array} data - Array that work as data source
     * @param {string} fileName - File name without extension
     * @param {object} config
     * alasql configuration settings, like:
     *  - (boolean) headers
     * more the highlighted below
     *  - (string) where
     *  - (string) orderBy
     *
     * filters defined
     *
     * formatDate(campo, [format])
     *
     * Example:
     *
     * var cars = [
     *  { name: 'Stilo',  brand: 'Fiat', date: moment().subtract(1, 'years') },
     *  { name: 'Punto',  brand: 'Fiat', date: moment().subtract(3, 'years') },
     *  { name: 'Fiesta', brand: 'Ford', date: '2017-04-01'}
     * ]
     *
     * C2File.exportToExcel([
     *  { name: 'brand', label: 'Brand' },
     *  { name: 'name', label: 'Nome' },
     *  { name: 'formatDate(date, \'DD-MM-YYYY\')', label: 'Bought in?' }
     * ], cars, 'data-export', {
     *  orderBy: 'brand ASC, name ASC',
     *  where: 'brand like "%Fi%"'
     * });
     *
     * @returns {promise} - returns a promise that can be or not resolved
     */
    function exportTo(fields, data, fileName, config) {
      var defaultConfig = {
        headers: true
      }

      if (angular.isObject(config)) angular.merge(defaultConfig, config);
      if (angular.isUndefined(fileName)) fileName = 'export'

      defaultConfig.where = (angular.isDefined(defaultConfig.where)) ? ' WHERE ' + defaultConfig.where : ' ';
      defaultConfig.orderBy = (angular.isDefined(defaultConfig.orderBy)) ? ' ORDER BY ' + defaultConfig.orderBy : ' ';

      fileName = fileName + '.' + defaultConfig.formatTo;

      return alasql.promise('SELECT ' + buildFields(fields)
        + ' INTO ' + defaultConfig.formatTo.toUpperCase() + '(?, ?) FROM ? '
        + defaultConfig.where
        + defaultConfig.orderBy, [fileName, defaultConfig, data]);
    }

    /**
     * Export the sent data as csv
     *
     * @see exportTo
     */
    function exportToCSV(fields, data, fileName, config) {
      if (angular.isUndefined(config)) config = {};

      config.formatTo = 'csv';

      return exportTo(fields, data, fileName, config);
    }

    /**
     * Export the sent data as excel
     *
     * @see exportTo
     */
    function exportToExcel(fields, data, fileName, config) {
      if (angular.isUndefined(config)) config = {};

      config.formatTo = 'xlsx';

      return exportTo(fields, data, fileName, config);
    }

    /**
     * Generate a string containing the fields to the SELECT da Query.
     */
    function buildFields(fields) {
      if (angular.isArray(fields)) {
        fields = lodash.map(fields, function(field) {
          return field.name + ' as [' + field.label + ']';
        }).join(', ');
      }

      return fields;
    }

    return obj;
  }

}());
