(function() {
  'use strict';

  angular
    .module('ngC2')
    .factory('Strings', Strings);

  /**
   * Return the default strings for the components label
   */
  function Strings() {
    var service = {
      yes: 'Yes',
      no: 'No',
      ok: 'Ok',
      total: 'Total',
      items: 'Items',
      todayText: 'Today',
      cancelText: 'Cancel'
    };

    return service;

  }
})();
