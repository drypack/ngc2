(function() {
  'use strict';

  angular
    .module('ngC2')
    .factory('DateFormat', DateFormat);

  /**
   * Return the default date and date time formats
   */
  function DateFormat() {
    var service = {
      ymd: 'YYYY/MM/DD',
      ymdhm: 'YYYY/MM/DD HH:mm'
    };

    return service;
  }
})();
