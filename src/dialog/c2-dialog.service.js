/*eslint-env es6*/

(function () {
  'use strict';

  angular.module('ngC2')
    .factory('C2Dialog', dialogService);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function dialogService($log, $mdDialog, $mdUtil, $rootScope, $animate, $document, Strings) {
    return {
      custom: custom,
      confirm: confirm,
      close: close,
      alert: alert
    };

    /**
     * Configures and do a merge between the passed object with the default settings
     * @param {object} config - Object containing the custom settings
     * @returns {dialogService} - Returns a service
     */
    function build(config) {

      if (!angular.isObject(config)) {
        $log.error('C2Dialog: invalid parameter. An object was expected.');
        return;
      }

      var defaultOptions = {
        hasBackdrop: false,
        escapeToClose: false,
        bindToController: true,
        clickOutsideToClose: true,
        autoWrap: true,
        skipHide: true,
        locals: {},
        zIndex: 75,
        fullscreen: false
      };

      // We don't do a merge with the local properties to avoid a the recursion limit
      if (angular.isDefined(config.locals)) {
        defaultOptions.locals = config.locals;
        delete config.locals;
      }

      return angular.merge(defaultOptions, config);
    }

    /**
     * Show an alert dialog and returns a promise that can be or not be resolved
     * @returns {promise}
     */
    function alert(config) {

      var options = build(config);

      options.locals = {
        title: (angular.isDefined(options.title) ? options.title : 'Exception'),
        description: (angular.isDefined(options.description) ? options.description : ''),
        okBgColor: (angular.isDefined(options.okBgColor) ? options.okBgColor : 'red-A700'),
        toolbarBgColor: (angular.isDefined(options.toolbarBgColor) ? options.toolbarBgColor : 'red-A700'),
        ok: (angular.isDefined(options.ok) ? options.ok : Strings.ok)
      };

      options.template =
          ` <md-dialog flex=50 aria-label="${options.locals.title}">
              <md-toolbar md-scroll-shrink md-colors="::{background:'default-{{ctrl.toolbarBgColor}}'}">
                <div class="md-toolbar-tools">
                  <h3>
                    <span>${options.locals.title}</span>
                  </h3>
                </div>
              </md-toolbar>
              <md-dialog-content layout-margin>
                <p>${options.locals.description}</p>
              </md-dialog-content>
              <md-dialog-actions>
                <md-button class="md-raised"
                  md-colors="::{background:'default-{{ctrl.okBgColor}}'}"
                  ng-click="ctrl.okAction()">${options.locals.ok}</md-button>
              </md-dialog-actions>
            </md-dialog>
          `;

      options.controller = ['$mdDialog', function($mdDialog) {
        var vm = this;

        vm.okAction = okAction;

        function okAction() {
          $mdDialog.hide();
        }
      }];

      options.controllerAs = 'ctrl';
      options.clickOutsideToClose = false;
      options.hasBackdrop = true;

      return $mdDialog.show(options);
    }

    /**
     * Show a confirmation dialog and return a promise that can be or not be resolved
     * @returns {promise}
     */
    function confirm(config) {

      var options = build(config);

      options.locals = {
        title: (angular.isDefined(options.title) ? options.title : ''),
        description: (angular.isDefined(options.description) ? options.description : ''),
        yesBgColor: (angular.isDefined(options.yesBgColor) ? options.yesBgColor : 'primary'),
        noBgColor: (angular.isDefined(options.noBgColor) ? options.noBgColor : 'accent'),
        yes: (angular.isDefined(options.yes) ? options.yes : Strings.yes),
        no: (angular.isDefined(options.no) ? options.no : Strings.no)
      };

      options.template =
          ` <md-dialog flex=50 aria-label="{{::ctrl.title}}">
              <md-toolbar md-scroll-shrink>
                <div class="md-toolbar-tools">
                  <h3>
                    <span>{{::ctrl.title}}</span>
                  </h3>
                </div>
              </md-toolbar>
              <md-dialog-content layout-margin>
                <p>{{::ctrl.description}}</p>
              </md-dialog-content>
              <md-dialog-actions>
                <md-button class="md-raised"
                  md-colors="::{background:'default-{{ctrl.yesBgColor}}'}"
                  ng-click="ctrl.yesAction()">${options.locals.yes}</md-button>
                <md-button class="md-raised"
                  md-colors="::{background:'default-{{ctrl.noBgColor}}'}"
                  ng-click="ctrl.noAction()">${options.locals.no}</md-button>
              </md-dialog-actions>
            </md-dialog>
          `;


      options.controller = ['$mdDialog', function($mdDialog) {
        var vm = this;

        vm.noAction = noAction;
        vm.yesAction = yesAction;

        function noAction() {
          $mdDialog.cancel();
        }
        function yesAction() {
          $mdDialog.hide();
        }
      }];

      options.controllerAs = 'ctrl';
      options.clickOutsideToClose = false;
      options.hasBackdrop = true;

      return $mdDialog.show(options);
    }

    /**
     * Show a custom dialog and return a promise that can be or not be resolved
     * @returns {promise}
     */
    function custom(config) {

      var options = build(config);

      if (angular.isUndefined(options.templateUrl) && angular.isUndefined(options.template)) {
        $log.error(
          'C2Dialog: templateUrl ou template undefined. It is expected an templateUrl or a template.');
        return;
      }

      // Here the backdrop is created manually to  para decrease the z-index through a css class
      // The z-index has to be smaller because the dialog.confirm uses the original z-index of 80
      options = addBackdrop(options);

      options.hasBackdrop = false;

      return $mdDialog.show(options);
    }

    /**
     * Create the backdrop and show the content in a configurable z-index
     * to override the existing elements in the screen
     */
    function addBackdrop(options) {
      if (options.hasBackdrop) {
        var backdrop = $mdUtil.createBackdrop($rootScope, 'md-dialog-backdrop md-opaque md-backdrop-custom');

        $animate.enter(backdrop, angular.element($document.find('body')));

        var originalOnRemoving = options.onRemoving;

        // Executed  when the dialog closing animation ends
        options.onRemoving = function () {
          backdrop.remove();
          if (angular.isFunction(originalOnRemoving)) originalOnRemoving.call();
        }

        var originalOnComplete = options.onComplete;

        // Executed when the dialog opening animation ends
        options.onComplete = function (scope, element) {
          var zIndex = parseInt(options.zIndex, 10);

          angular.element($document[0].querySelector('.md-backdrop-custom')).css('z-index', zIndex);
          element.css('z-index', zIndex + 1);
          if (angular.isFunction(originalOnComplete)) originalOnComplete.call();
        }
      }

      return options;
    }

    /**
     * Close the dialog
     */
    function close() {
      $mdDialog.hide();
    }
  }
})();
