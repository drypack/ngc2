/*eslint-env es6*/

(function() {
  'use strict';

  angular
    .module('ngC2')
    .factory('C2Toast', Toast);

  /**
   * Encapsulate the angular toast component adding custom options
   */
  /** @ngInject */
  function Toast($mdToast, lodash, $log, C2Dialog) {
    var obj = {
      success: success,
      error: error,
      errorValidation: errorValidation,
      info: info,
      warn: warn,
      hide: hide
    };

    /**
     * Show a notification
     *
     * @param {string} msg - Message to be shown
     * @param {string} color - Message background color
     * @returns {promisse} - return promise that can be or not resolved
     */
    function toast(msg, color, options) {
      if (msg) {
        var defaultOptions = {
          template: `
            <md-toast>
              <div class="md-toast-content" md-colors="::{background:'${color}'}">
                <span class="md-toast-text" flex>${msg}</span>
              </div>
            </md-toast>
          `,
          position: 'top right'
        };

        if (angular.isObject(options)) defaultOptions = angular.merge(defaultOptions, options);

        return $mdToast.show(defaultOptions);
      } else {
        $log.debug('Message to be shown not passed');
      }
    }

    /**
     * Hide a notification
     *
     * @param {object} object - An additional argument to resolve the promise
     *
     * @returns {promise} - Returns a promise that is executed when the notification is removed from the DOM
     */
    function hide(object) {
      return $mdToast.hide(object);
    }

    function success(msg, options) {
      return toast(msg, 'green', options);
    }

    /**
     * Show a toast with error messages related to validation
     *
     * @param {string | object | array} errors - String, Object or Array containing the error messages
     * Supports two different formats
     *
     * To validate attributes
     *   {
     *    "attribute-name1": ["error message 1", "error message 2"],
     *    "attribute-name2": ["error message 1"]
     *   }
     *
     * or - To multiple simple messages
     *   [
     *    'Message 1',
     *    'Message 2'
     *   ]
     *
     *  or - for a single Message
     *   "Message 1"
     *
     *  or - for exceptions - in this case a C2Dialog
     *   {
     *     message: 'Unknown Exception',
     *     source: 'DryPack.Controllers.SupportController.langs()',
     *     line: 21,
     *     stacktrace: 'at DryPack.Controllers.SupportController.langs() in /home/workspa...'
     *   }
     * @param {string} msg - Optional. error Message
     */
    function error(errors, options = {}) {
      // if it is an object containing error, iterate over its attributes
      // to show the error messages assigned
      if (angular.isObject(errors)) {
        var errorStr = '';

        if (checkErrorIsUnknownException(errors)) {
          errorStr += 'Message: ' + errors.message
            + '<br/>Source: ' + errors.source
            + '<br/>Line: ' + errors.line
            + '<br/><br/>StackTrace: ';

          var steps = errors.stacktrace.split(' at ');

          if (steps.length > 0) {
            steps.forEach(function(step, index) {
              errorStr +=  '<br/>Step ' + (index + 1) + ': ' + step;
            });
          } else {
            errorStr +=  errors.stacktrace;
          }

          return C2Dialog.alert({
            title: 'Exception',
            description: errorStr
          });
        } else {
          // Show the error messages contained in the object
          //  {
          //    "name": ["the field is mandatory"],
          //    "password": [
          //      " The password confirmation doe snot match.",
          //      " The password must have at least 6 characters."
          //    ]
          //  }
          lodash.forIn(errors, function(keyErrors) {
            errorStr += buildArrayMessage(keyErrors);
          });
        }

        errors = errorStr;
      } else {
        if (angular.isArray(errors)) {
          errors = buildArrayMessage(errors);
        }
      }

      return toast(errors, 'red-A700', options);
    }

    function buildArrayMessage(arrMsg) {
      var msg =  '';

      if (angular.isArray(arrMsg)) {
        // Iterate over the attribute errors
        arrMsg.forEach(function(error) {
          msg += error + '<br/>';
        });
      } else {
        msg += arrMsg + '<br/>';
      }

      return msg;
    }

    /**
     * show a toast with the validation error messages related to it
     *
     * @param {object | array} errors - Object or Array containing the error messages. Must be in the following format:
     *   {
     *    "attribute-name1": ["error message 1", "error message 2"],
     *    "attribute-name2": ["error message 1"]
     *   }
     * or
     *   [
     *    'Message 1',
     *    'Message 2'
     *   ]
     * @param {string} msg - Optional. Error message.
     * @param {object} options - default $mdToast options
     */
    function errorValidation(errors, msg, options) {
      obj.error((angular.isArray(errors) || angular.isObject(errors)) ? errors : msg, options);
    }

    function info(msg, options) {
      return toast(msg, 'teal', options);
    }

    function warn(msg, options) {
      return toast(msg, 'warn', options);
    }

    function checkErrorIsUnknownException(errors) {
      return (angular.isObject(errors)
        && angular.isDefined(errors.line)
        && angular.isDefined(errors.source)
        && angular.isDefined(errors.stacktrace));
    }

    return obj;
  }

}());
