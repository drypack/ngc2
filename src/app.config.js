(function () {
  'use strict';

  angular
    .module('ngC2')
    .config(config);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function config($mdThemingProvider, $mdIconProvider,
    moment, C2Icons, $qProvider) {
    // Default settings for the components/libs used
    $mdThemingProvider.theme('default')
      .primaryPalette('indigo')
      .accentPalette('blue-grey');

    moment.locale('en-US');

    moment.createFromInputFallback = function(config) {
      // unreliable string magic, or
      config._d = new Date(config._i);
    };

    $qProvider.errorOnUnhandledRejections(false);

    angular.forEach(C2Icons, function(icon) {
      $mdIconProvider.icon(icon.id, icon.url);
    });
  }
}());
