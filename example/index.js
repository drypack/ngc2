angular.module('app', ['ngC2'])
  .controller('GlobalController', GlobalController);

GlobalController.$inject = ['C2Pagination', 'C2Spinner', 'C2Toast', 'C2Dialog', '$timeout', 'FileUploader', 'C2File'];

function GlobalController(C2Pagination, C2Spinner, C2Toast, C2Dialog, $timeout, FileUploader, C2File) {
  var vm = this;

  vm.uploader = new FileUploader();

  vm.search = search;
  vm.exportToExcel = exportToExcel;
  vm.notification = notification;
  vm.hideNotification = hideNotification;
  vm.customDialog = customDialog;
  vm.confirmDialog = confirmDialog;
  vm.notificationArrayValidationErrors = notificationArrayValidationErrors;
  vm.notificationObjectValidationErrors = notificationObjectValidationErrors;
  vm.notificationExceptionErrors = notificationExceptionErrors;

  activate();

  function activate() {

    vm.cars = [];
    vm.carsData = [{
      brand: 'Fiat',
      name: 'Stilo',
      date: moment()
    }, {
      brand: 'Fiat',
      name: 'Uno',
      date: moment("2015-1-1")
    }, {
      brand: 'GM',
      name: 'Onix',
      date: new Date()
    }, {
      brand: 'Ford',
      name: 'Fiesta',
      date: '2017-04-01'
    }, {
      brand: 'GM',
      name: 'Vectra',
      date: moment().subtract(7, 'days')
    }, {
      brand: 'GM',
      name: 'Cruze',
      date: moment().subtract(7, 'years')
    }, {
      brand: 'Ford',
      name: 'Ka',
      date: moment().subtract(1, 'years')
    }, {
      brand: 'Wolks',
      name: 'Fusca',
      date: moment().subtract(2, 'years')
    }, {
      brand: 'GM',
      name: 'Agile',
      date: moment().subtract(3, 'years')
    }, {
      brand: 'Fiat',
      name: 'Punto',
      date: moment().subtract(4, 'years')
    }, {
      brand: 'GM',
      name: 'S10',
      date: moment().subtract(1, 'years')
    }];

    vm.carsData.sort(function (previousCar, nextCar) {
      if (previousCar.name < nextCar.name)
        return -1;
      if (previousCar.name > nextCar.name)
        return 1;
      return 0;
    });

    vm.maxDate = moment().add(1,'y');

    //crie uma instancia e informe a
    //função responsável pela busca e quantidade por página a ser exibido

    vm.paginator = C2Pagination.getInstance(search, 3);

    search(1);
  }

  function search(page) {
    vm.paginator.currentPage = page; //atualize a página atual
    C2Spinner.show();

    $timeout(function () {
      var offset = (page - 1) * vm.paginator.perPage;

      vm.cars = vm.carsData.slice(offset, offset + vm.paginator.perPage);
      //chame a função para calcular a quantidade de página
      vm.paginator.calcNumberOfPages(vm.carsData.length);

      C2Spinner.hide();
    }, 1000)
  }

  function exportToExcel() {
    C2File.exportToExcel([
      { name: 'brand', label: 'Brand' },
      { name: 'name', label: 'Name' },
      { name: 'formatDate(date, \'YYYY-MM-DD\')', label: 'Bought in?' }
    ], vm.carsData, 'data-export', {
      orderBy: 'brand ASC, name ASC'
    });
  }

  function notification(type, message) {
    C2Toast[type](message);
  }

  function hideNotification() {
    C2Toast.hide();
  }

  function notificationArrayValidationErrors() {
    C2Toast.errorValidation([
      'Attribute 1 is mandatory',
      'Attribute 1 must be in the format 999.999.999-99',
      'Attribute 2 must have up to 10 characters'
    ]);
  }

  function notificationObjectValidationErrors() {
    C2Toast.errorValidation({
     'Attribute 1': ['is mandatory', 'must be in the format 999.999.999-99'],
     'Attribute 2': ['must have up to 10 characters']
    });
  }

  function notificationExceptionErrors() {
    C2Toast.errorValidation({
      message: 'Unknown Exception',
      source: 'DryPack.Controllers.SupportController.langs()',
      line: 21,
      stacktrace: 'at DryPack.Controllers.SupportController.langs() in /home/user/workspace/projects/dotnet/drypack/Controllers/SupportController.cs:line 21 at Microsoft.AspNetCore.Mvc.Internal.ControllerActionInvoker.<InvokeActionMethodAsync>d__27.MoveNext() --- End of stack trace from previous location where exception was thrown --- at System.Runtime.ExceptionServices.ExceptionDispatchInfo.Throw() at System.Runtime.CompilerServices.TaskAwaiter.HandleNonSuccessAndDebuggerNotification(Task task) at Microsoft.AspNetCore.Mvc.Internal.ControllerActionInvoker.<InvokeNextActionFilterAsync>d__25.MoveNext() --- End of stack trace from previous location where exception was thrown --- at System.Runtime.ExceptionServices.ExceptionDispatchInfo.Throw() at Microsoft.AspNetCore.Mvc.Internal.ControllerActionInvoker.Rethrow(ActionExecutedContext context) at Microsoft.AspNetCore.Mvc.Internal.ControllerActionInvoker.Next(State& next, Scope& scope, Object& state, Boolean& isCompleted) at Microsoft.AspNetCore.Mvc.Internal.ControllerActionInvoker.<InvokeNextExceptionFilterAsync>d__24.MoveNext()"'
    });
  }

  function customDialog() {
    var config = {
      controller: function () {
        var vm = this;

        vm.title = 'Custom dialog title';
        vm.description = 'Custom dialog content';

        vm.close = vm.close;

        vm.close = function () {
          C2Dialog.close();
        }

      },
      controllerAs: 'ctrl',
      templateUrl: 'custom-dialog.html',
      hasBackdrop: true
    };

    C2Dialog.custom(config);

  }

  function confirmDialog() {
    var config = {
      title: 'Confirmation dialog',
      description: 'Confirmation dialog description'
    };

    C2Dialog.confirm(config).then( () => {
      console.log("You have clicked in the yes button.");
    });
  }

}
